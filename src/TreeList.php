<?php

namespace Lliure\TreeList;

use ArrayAccess;
use Countable;
use Iterator;

class TreeList implements ArrayAccess, Iterator, Countable
{

	protected string $id = 'id';
	protected string $ancestor = 'parentId';
	protected string $downward = 'children';
	protected array $tree = [];
	protected array $array = [];
	
	/**
	 * @param array|ArrayAccess $list
	 * @param string            $id
	 * @param string            $ancestor
	 * @param string            $downward
	 */
	public function __construct($list, string $id = 'id', string $ancestor = 'parentId', string $downward = 'children')
	{
		$this->id = $id;
		$this->ancestor = $ancestor;
		$this->downward = $downward;
		$this->tree = $list;
		$this->buildTree($this->tree);
	}

	private function buildTree(&$list){
		$list = array_combine(array_column($list, $this->id), $list);

		foreach($list as $id => &$node){
			if($node instanceof Member){
				continue;
			}

			$node = new Member($node, $this->id, $this->downward, $this->getParent($list, ($node[$this->ancestor] ?? null)));

			if($node->parent() === null){
				$this->array[$id] =& $node;
			}
		}
	}
	
	private function &getParent(&$list, $parentId): ?Member{
		if(!isset($list[$parentId])){
			$parent = null;
			return $parent;
		}

		$parent =& $list[$parentId];

		if($parent instanceof Member){
			return $parent;
		}

		$parent = new Member($parent, $this->id, $this->downward, $this->getParent($list, ($parent[$this->ancestor] ?? null)));
		return $parent;
	}
	
	/**
	 * @return array
	 */
	public function toArray(): array{
		return array_map(function(Member $member){
			return $member->toArray();
		}, $this->array);
	}
	
	/**
	 * @param $offset
	 * @return bool
	 */
	public function offsetExists($offset): bool{
		return isset($this->tree[$offset]);
	}
	
	/**
	 * @param $offset
	 * @return Member
	 */
	public function offsetGet($offset){
		return $this->tree[$offset];
	}
	
	/**
	 * @throws \Exception
	 */
	public function offsetSet($offset, $value){
		throw new \Exception('TreeList is read only');
	}
	
	/**
	 * @throws \Exception
	 */
	public function offsetUnset($offset){
		throw new \Exception('TreeList is read only');
	}
	
	/**
	 * @return false|Member
	 */
	public function current(){
		return current($this->array);
	}
	
	/**
	 * @return void
	 */
	public function next(){
		next($this->array);
	}
	
	/**
	 * @return bool|float|int|mixed|string|null
	 */
	public function key(){
		return key($this->array);
	}
	
	/**
	 * @return bool
	 */
	public function valid(){
		return $this->key() !== null;
	}
	
	/**
	 * @return void
	 */
	public function rewind(){
		reset($this->array);
	}

	/**
	 * @return int
	 */
	public function count(){
		return count($this->array);
	}
}