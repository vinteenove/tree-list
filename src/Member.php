<?php

namespace Lliure\TreeList;

class Member implements \ArrayAccess
{

	protected $data;
	protected ?Member $parent = null;
	protected string $downward;
	protected array $children;
	
	/**
	 * Member constructor.
	 *
	 * @param array       $data
	 * @param string      $id
	 * @param string      $downward
	 * @param Member|null $parent
	 */
	public function __construct($data, string $id, string $downward, ?Member &$parent = null)
	{
		$this->data = $data;
		$this->parent =& $parent;
		$this->downward = $downward;
		$this->children = [];
		$this->data[$downward] =& $this->children;

		if($this->parent){
			$parent->setChildren($this->data[$id], $this);
		}
	}

	private function setChildren($key, Member $children): void{
		$this->children[$key] = $children;
	}
	
	/**
	 * @param int|null $levels
	 * @return array
	 */
	public function children(?int $levels = 1): array{
		$levels = ((is_null($levels))? $levels: abs($levels) - 1);

		if($levels !== null && $levels < 0){
			return [];
		}

		if($levels === 0){
			return $this->children;
		}

		return array_merge($this->children, ...array_map(fn(Member &$children) => $children->children($levels), $this->children));
	}
	
	/**
	 * @param int|null $levels
	 * @return Member[]
	 */
	public function parents(?int $levels = null): array{
		$levels = ((is_null($levels))? $levels: abs($levels) - 1);

		if(($levels !== null && $levels < 0) || $this->parent == null){
			return [];
		}

		if($levels === 0){
			return [$this->parent];
		}

		return array_merge(($this->parent->parents($levels) ?? []), [$this->parent]);
	}
	
	/**
	 * @return Member|null
	 */
	public function parent(): ?Member{
		return $this->parent;
	}
	
	/**
	 * @return int
	 */
	public function level(): int{
		if($this->parent == null){
			return 0;
		}
		return $this->parent->level() + 1;
	}
	
	/**
	 * @param $offset
	 * @return bool
	 */
	public function offsetExists($offset){
		return isset($this->data[$offset]);
	}

	/**
	 * @param $offset
	 * @return mixed|null
	 */
	public function offsetGet($offset){
		return $this->data[$offset] ?? null;
	}
	
	/**
	 * @param $offset
	 * @param $value
	 * @return void
	 */
	public function offsetSet($offset, $value){
		if (is_null($offset)) {
			$this->data[] = $value;
		} else {
			$this->data[$offset] = $value;
		}
	}
	
	/**
	 * @param $offset
	 * @return void
	 */
	public function offsetUnset($offset){
		unset($this->data[$offset]);
	}

	/**
	 * @return array
	 */
	public function toArray(): array{
		$data = $this->data;
		unset($data[$this->downward]);
		$data[$this->downward] = array_map(fn(Member &$children) => $children->toArray(), $this->children);
		if(empty($data[$this->downward])){
			unset($data[$this->downward]);
		}
		return $data;
	}

}